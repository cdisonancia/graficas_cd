# Gráficas_CD

Repositorio de gráficas producidas por el [Colectivo Disonancia](colectivodisonancia.net) para publicaciones y otros contenidos.



## Internet Archive

Algunas de estas imágenes también están disponibles en nuestra cuenta de [Internet Archive](https://archive.org/details/@colectivo_disonancia).



## Licencia

Contenidos bajo licencia [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es_ES).

El texto de la licencia se encuentra en [Licence](https://gitlab.com/cdisonancia/graficas_cd/-/blob/master/LICENSE).

